<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="index.php"><img src="img/logo.png" alt="logo odalys" width="90px"> </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse order-3 dual-collapse2" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      </ul>
      <div class="d-flex ">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-menu">
            <li class="nav-item">
              <a class="nav-link <?php if(isset($page)) echo 'active-menu' ?>" aria-current="page" href="page.php">Vos envies</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Bons Plans</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Catalogues</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Newsletter</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="far fa-question-circle me-2"></i>Aide</a>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle <?php if(isset($homepage)) echo 'active-menu' ?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    fr - €
                </a>
                <!-- <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Action 1</a></li>
                    <li><a class="dropdown-item" href="#">Action 3</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="#">Action 2</a></li>
                </ul> -->
            </li>
          </ul>
      </div>
    </div>

  </div>
</nav>
