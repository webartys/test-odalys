<div id="carouselExampleCaptions" class="carousel slide text-theme-1" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="img/slider/01.jpg" class="d-block w-100" alt="slider 01">
            <div class="carousel-caption d-none d-md-block text-carousel-position text-start p-4">
                <p class="text-color text-theme-2">Premières minutes hiver 2020/2021<p/>
                <p class="h3 text-shadow">Réservez des maintenant, </p>
                <p class="h1 text-theme-3 text-shadow">Vous ne prenez <br> aucun risque !</p>
                <p class="mb-1"><span class="text-bg-orange p-1">Assurance annulation : offerte</span></p>
                <p class="mb-1"><span class="text-bg-orange p-1">Garantie neige : offerte</span> </p>
                <p class="mb-4"><span class="text-bg-orange p-1">frais de dossiers : offerts</span></p>

                <a href="#" class="btn-trans text-uppercase mt-5 text-shadow">J'en profite <i class="fas fa-long-arrow-alt-right ms-2"></i></a>
            </div>
        </div>
        <div class="carousel-item">
            <img src="img/slider/02.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block text-carousel-position text-start p-4">
                <p class="text-color text-theme-2">Premières minutes hiver 2020/2021<p/>
                <p class="h3 text-shadow">Réservez des maintenant, </p>
                <p class="h1 text-theme-3 text-shadow">Vous ne prenez <br> aucun risque !</p>
                <p class="mb-1"><span class="text-bg-orange p-1">Assurance annulation : offerte</span></p>
                <p class="mb-1"><span class="text-bg-orange p-1">Garantie neige : offerte</span> </p>
                <p class="mb-4"><span class="text-bg-orange p-1">frais de dossiers : offerts</span></p>

                <a href="#" class="btn-trans text-uppercase mt-5 text-shadow">J'en profite <i class="fas fa-long-arrow-alt-right ms-2"></i></a>
            </div>
        </div>
        <div class="carousel-item">
            <img src="img/slider/03.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block text-carousel-position text-start p-4">
                <p class="text-color text-theme-2">Premières minutes hiver 2020/2021<p/>
                <p class="h3 text-shadow">Réservez des maintenant, </p>
                <p class="h1 text-theme-3 text-shadow">Vous ne prenez <br> aucun risque !</p>
                <p class="mb-1"><span class="text-bg-orange p-1">Assurance annulation : offerte</span></p>
                <p class="mb-1"><span class="text-bg-orange p-1">Garantie neige : offerte</span> </p>
                <p class="mb-4"><span class="text-bg-orange p-1">frais de dossiers : offerts</span></p>

                <a href="#" class="btn-trans text-uppercase mt-5 text-shadow">J'en profite <i class="fas fa-long-arrow-alt-right ms-2"></i></a>
            </div>
        </div>
    </div>
    <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button> -->
</div>
