
<div class="items">
    <div class="bg-white rounded">
        <img src="img/weeks/01.jpg" class="rounded-top">
        <div class="p-2">
            <span class="text-muted">Provence et luberon</span>
            <p>Résidence Oladys le Mas des Alpilles  </p>

            <span class="text-muted"> A partir de </span> <span class="text-decoration-line-through text-muted">200€</span> <br>
            <span class="text-bg-orange p-1">-10%</span> <span class="text-orange text-theme-2 h3 align-middle" >180€</span> <br>
            <p><span class="text-start">Arrivée le 26/08/2022 - 7 nuits</span> <i class="fas fa-2x fa-long-arrow-alt-right text-color ms-5 align-middle"></i></p>
        </div>
    </div>
    <div class="bg-white rounded">
        <img src="img/weeks/02.jpg" class="rounded-top">
        <div class="p-2">
            <span class="text-muted">Languedoc Roussillon -  Calvission</span>
            <p>Résidence Club Oladys le Mas des Vignes  </p>

            <span class="text-muted"> A partir de </span> <br>
            <span class="text-color text-theme-2 h3 align-middle" >208€</span> <br>
            <p><span class="text-start">Arrivée le 26/08/2022 - 7 nuits</span> <i class="fas fa-2x fa-long-arrow-alt-right text-color ms-5 align-middle"></i></p>
        </div>
    </div>
    <div class="bg-white rounded">
        <img src="img/weeks/03.jpg" class="rounded-top">
        <div class="p-2">
            <span class="text-muted">Provence & luberon - Gréoux les Bains</span>
            <p>Résidence Oladys coté Provence & la Licorne  </p>

            <span class="text-muted"> A partir de </span> <span class="text-decoration-line-through text-muted">250€</span> <br>
            <span class="text-bg-orange p-1">-20%</span> <span class="text-orange text-theme-2 h3 align-middle" >200€</span> <br>
            <p><span class="text-start">Arrivée le 26/08/2022 - 7 nuits</span> <i class="fas fa-2x fa-long-arrow-alt-right text-color ms-5 align-middle"></i></p>
        </div>
    </div>
    <div class="bg-white rounded">
        <img src="img/weeks/04.jpg" class="rounded-top">
        <div class="p-2">
            <span class="text-muted">Languedoc Roussillon -  Calvission</span>
            <p>Résidence Oladys coté Provence  </p>

            <span class="text-muted"> A partir de </span> <br>
            <span class="text-color text-theme-2 h3 align-middle" >216€</span> <br>
            <p><span class="text-start">Arrivée le 26/08/2022 - 7 nuits</span> <i class="fas fa-2x fa-long-arrow-alt-right text-color ms-5 align-middle"></i></p>
        </div>
    </div>
    <div class="bg-white rounded">
        <img src="img/weeks/05.jpg" class="rounded-top">
        <div class="p-2">
            <span class="text-muted">Provence et luberon</span>
            <p>Résidence Oladys coté Provence & la Licorne  </p>

            <span class="text-muted"> A partir de </span> <span class="text-decoration-line-through text-muted">250€</span> <br>
            <span class="text-bg-orange p-1">-10%</span> <span class="text-orange text-theme-2 h3 align-middle" >225€</span> <br>
            <p><span class="text-start">Arrivée le 26/08/2022 - 7 nuits</span> <i class="fas fa-2x fa-long-arrow-alt-right text-color ms-5 align-middle"></i></p>
        </div>
    </div>
    <div class="bg-white rounded">
        <img src="img/weeks/06.jpg" class="rounded-top">
        <div class="p-2">
            <span class="text-muted">Languedoc Roussillon -  Calvission</span>
            <p>Résidence Oladys coté Provence  </p>

            <span class="text-muted"> A partir de </span> <br>
            <span class="text-color text-theme-2 h3 align-middle" >227€</span> <br>
            <p><span class="text-start">Arrivée le 26/08/2022 - 7 nuits</span> <i class="fas fa-2x fa-long-arrow-alt-right text-color ms-5 align-middle"></i></p>
        </div>
    </div>


</div>
