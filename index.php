<?php $homepage = true ?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="test odalys">
        <meta name="keywords" content="odalys, bootstrap, app, theme">
        <meta name="author" content="tasca">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="icon" href="img/favicon.png">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css">

        <link rel="stylesheet" href="libs/weeks/weeks.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

        <link rel="stylesheet" href="css/design.css">

        <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script src="libs/weeks/weeks.js"></script>

        <title>odalys-vacances</title>
    </head>
    <body class="bg-light">

        <?php include_once('inc/menu.php') ?>
        <?php include_once('inc/caroussel.php') ?>
        <?php include_once('inc/form.php') ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="card p-3 mb-3">
                          <figure class="p-3 mb-0">
                            <blockquote class="blockquote">
                                <p class="text-center"><i class="far fa-3x fa-lightbulb text-color"></i></p>
                              <p class="text-color text-theme-3 h3 text-center">Nos idées de location de Vacances</p>
                            </blockquote>
                            <figcaption class="mb-0 text-theme-1 text-color text-center">
                              Besoin d'inspitation pour vos vacances ? Laissez-vous guider !
                            </figcaption>
                          </figure>
                        </div>
                        <div class="mb-3 bg-img" style="background-image:url('img/middle/01.jpg'); position:relative">
                                <p class="text-img-top text-white text-shadow">
                                    <span class="text-theme-2">La campagne</span> <br>
                                    <span class="text-theme-1" style="font-size:11px">ça vous gagne !</span>
                                </p>
                                <span class="text-img-bottom text-bg-orange">Jusqu'a -30%</span>
                                <span class="text-img-bottom-right text-white text-shadow"><i class="fas fa-2x fa-long-arrow-alt-right ms-2"></i> </span>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="mb-3 bg-img-2" style="background-image:url('img/middle/04.jpg'); position:relative">
                            <p class="text-img-top text-white text-shadow">
                                <span class="text-theme-2">Printemps</span> <br>
                                <span class="text-theme-1" style="font-size:11px">Préparez l'arrivée des beaux jours</span>
                            </p>
                            <span class="text-img-bottom text-bg-orange">Jusqu'a -30%</span>
                            <span class="text-img-bottom-right text-white text-shadow"><i class="fas fa-2x fa-long-arrow-alt-right ms-2"></i> </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="mb-3 bg-img"  style="background-image:url('img/middle/02.jpg'); position:relative">
                            <p class="text-img-top text-white text-shadow">
                                <span class="text-theme-2">Espace bien-être</span> <br>
                                <span class="text-theme-1" style="font-size:11px">Place à la détente</span>
                            </p>
                            <span class="text-img-bottom text-bg-orange">Jusqu'a -15%</span>
                            <span class="text-img-bottom-right text-white text-shadow"><i class="fas fa-2x fa-long-arrow-alt-right ms-2"></i> </span>
                        </div>
                        <div class="mb-3 bg-img"  style="background-image:url('img/middle/03.jpg'); position:relative">
                            <p class="text-img-top text-white text-shadow">
                                <span class="text-theme-2">Forfaits remontées <br> mécaniques</span>
                            </p>
                            <span class="text-img-bottom-right text-white text-shadow"><i class="fas fa-2x fa-long-arrow-alt-right ms-2"></i> </span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <div class="mb-3 bg-img-2" style="background-image:url('img/middle/05.jpg'); position:relative">
                            <p class="text-img-top text-white text-shadow">
                                <span class="text-theme-2">Août</span> <br>
                                <span class="text-theme-1" style="font-size:11px">Faire le plein de Soleil</span>
                            </p>
                            <span class="text-img-bottom-right text-white text-shadow"><i class="fas fa-2x fa-long-arrow-alt-right ms-2"></i> </span>
                        </div>
                    </div>

                </div>

                <div class="row mt-4 mb-4">
                    <span class="title-we text-uppercase mx-auto text-center">Le calendrier des longs Week-end</span>
                    <?php include_once('inc/weeks.php') ?>

                </div>

            </div>


            <section class="">
                <!-- Footer -->
                <footer class="text-center bg-footer text-white pt-5 pb-5" style="background-color: #0a4275;">
                    <div class="row">
                        <div class="col-md-6 col-12 text-md-end text-center mb-3">
                            <img src="img/logo-white.png" alt="logo" width="70px"><br>
                            <span class="text-uppercase text-trans" style="font-size:12px">vacances</span>
                        </div>
                        <div class="col-md-6 col-12 text-md-start text-center mb-3">
                            <p class="text-theme-2">A propos d'Odalys</p>
                            <a class="text-trans" style="font-size:12px; text-decoration:none">Qui sommes nous ? </a> <br>
                            <a class="text-trans" style="font-size:12px; text-decoration:none">Nous contacter </a> <br>
                            <a class="text-trans" style="font-size:12px; text-decoration:none">Conditions de vente </a> <br>
                            <a class="text-trans" style="font-size:12px; text-decoration:none">Données Personnelles </a> <br>
                            <a class="text-trans" style="font-size:12px; text-decoration:none">Aides et FAQ </a> <br>
                            <a class="text-trans" style="font-size:12px; text-decoration:none">Mentions Légales </a>
                        </div>
                    </div>

                </footer>
                <!-- Footer -->
            </section>





        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" async></script>

    </body>
</html>
